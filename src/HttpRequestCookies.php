<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7Cookies;

use BadMethodCallException;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Reads cookies from a {@see ServerRequestInterface}.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class HttpRequestCookies
{
    /**
     * The request object the cookies will be read from.
     */
    private ServerRequestInterface $request;

    /**
     * Constructor.
     *
     * @param ServerRequestInterface $request
     *        The request object the cookies will be read from.
     * @no-named-arguments
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Returns true if the specified cookie exists, otherwise false.
     *
     * @param string $name
     *        The name of the cookie.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function hasCookie(string $name): bool
    {
        return array_key_exists($name, $this->request->getCookieParams());
    }

    /**
     * Get the value of the specified cookie, returns an empty string if the cookie does not exist.
     *
     * @param string $name
     *        The name of the cookie.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getValue(string $name): string
    {
        if (!$this->hasCookie($name)) {
            return '';
        }

        return (string)$this->request->getCookieParams()[$name];
    }

    /**
     * Get the original request object.
     *
     * @return ServerRequestInterface
     * @api usage
     * @since 1.0
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }
}
