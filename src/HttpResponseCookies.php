<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7Cookies;

use BadMethodCallException;
use DateTime;
use DateTimeImmutable;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Sets cookies in a {@see ResponseInterface}.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class HttpResponseCookies
{
    /**
     * The response object the cookies will be added to.
     */
    private ResponseInterface $response;

    /**
     * Duplicate cookies have been removed from the response.
     */
    private bool $cleaned = false;

    /**
     * Constructor.
     *
     * @param ResponseInterface $response
     *        The response object the cookies will be modified on.
     * @no-named-arguments
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Set the specified cookie until the browser is closed.
     *
     * @param string $name
     *        The name of the cookie.
     * @param string $value
     *        The value of the cookie.
     * @param string $path
     *        The path the cookie is valid for.
     * @param string $domain
     *        The domain the cookie is valid for.
     * @param bool $secure
     *        Only send the cookie using https.
     * @param bool $httpOnly
     *        Prevent the cookie to be sent using javascript.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function setSessionCookie(
        string $name,
        string $value,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false
    ): self {
        $this->addCookieToResponse(
            $name,
            $value,
            0,
            $path,
            $domain,
            $secure,
            $httpOnly,
        );

        return $this;
    }

    /**
     * Set the specified cookie until the specified moment.
     *
     * If the specified moment is in the past, the cookie will be removed.
     *
     * @param string $name
     *        The name of the cookie.
     * @param string $value
     *        The value of the cookie.
     * @param DateTimeImmutable $until
     *        The moment until the cookie should be valid.
     * @param string $path
     *        The path the cookie is valid for.
     * @param string $domain
     *        The domain the cookie is valid for.
     * @param bool $secure
     *        Only send the cookie using https.
     * @param bool $httpOnly
     *        Prevent the cookie to be sent using javascript.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function setCookieUntil(
        string $name,
        string $value,
        DateTimeImmutable $until,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false
    ): self {
        $maxAge = $until->getTimestamp() - time();

        if ($maxAge < 1) {
            $maxAge = -1;
        }

        $this->addCookieToResponse(
            $name,
            $value,
            $maxAge,
            $path,
            $domain,
            $secure,
            $httpOnly,
        );

        return $this;
    }

    /**
     * Set the specified cookie for the specified number of seconds.
     *
     * @param string $name
     *        The name of the cookie.
     * @param string $value
     *        The value of the cookie.
     * @param int $seconds
     *        The number of seconds the cookie should be valid, must be greater than or equal to zero.
     * @param string $path
     *        The path the cookie is valid for.
     * @param string $domain
     *        The domain the cookie is valid for.
     * @param bool $secure
     *        Only send the cookie using https.
     * @param bool $httpOnly
     *        Prevent the cookie to be sent using javascript.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function setCookieFor(
        string $name,
        string $value,
        int $seconds,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false
    ): self {
        $seconds = max([0, $seconds]);

        $this->addCookieToResponse(
            $name,
            $value,
            $seconds,
            $path,
            $domain,
            $secure,
            $httpOnly,
        );

        return $this;
    }

    /**
     * Remove the specified cookie.
     *
     * @param string $name
     *        The name of the cookie.
     * @param string $path
     *        The path the cookie is valid for.
     * @param string $domain
     *        The domain the cookie is valid for.
     * @param bool $secure
     *        Only send the cookie using https.
     * @param bool $httpOnly
     *        Prevent the cookie to be sent using javascript.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function unsetCookie(
        string $name,
        string $path = '',
        string $domain = '',
        bool $secure = false,
        bool $httpOnly = false
    ): self {
        $this->addCookieToResponse(
            $name,
            '',
            -1,
            $path,
            $domain,
            $secure,
            $httpOnly,
        );

        return $this;
    }

    /**
     * Add the specified cookie with the specified parameters to the response.
     *
     * @param string $name
     *        The name of the cookie.
     * @param string $value
     *        The value of the cookie.
     * @param int $maxAge
     *        The maximum age of the cookie in seconds.
     * @param string $path
     *        The path the cookie is valid for.
     * @param string $domain
     *        The domain the cookie is valid for.
     * @param bool $secure
     *        Only send the cookie using https.
     * @param bool $httpOnly
     *        Prevent the cookie to be sent using javascript.
     * @no-named-arguments
     */
    private function addCookieToResponse(
        string $name,
        string $value,
        int $maxAge,
        string $path,
        string $domain,
        bool $secure,
        bool $httpOnly
    ): void {
        if (!self::isValidCookieName($name)) {
            $msg = sprintf(
                'Invalid cookie name: %1$s',
                $name,
            );
            throw new InvalidArgumentException($msg);
        }

        $cookie = [];
        $cookie[] = $name . '=' . rawurlencode($value);

        if ($maxAge < 0) {
            $cookie[] = 'Expires=' . gmdate(DateTime::COOKIE, 0);
            $cookie[] = 'Max-Age=0';
        } elseif ($maxAge > 0) {
            $cookie[] = 'Expires=' . gmdate(DateTime::COOKIE, time() + $maxAge);
            $cookie[] = 'Max-Age=' . $maxAge;
        }

        if ($path !== '') {
            $cookie[] = 'Path=' . implode('/', array_map('rawurlencode', explode('/', $path)));
        }

        if ($domain !== '') {
            $cookie[] = 'Domain=' . rawurlencode($domain);
        }

        if ($secure) {
            $cookie[] = 'Secure';
        }

        if ($httpOnly) {
            $cookie[] = 'HttpOnly';
        }

        $this->response = $this->response->withAddedHeader('Set-Cookie', implode(';', $cookie));

        $this->cleaned = false;
    }

    /**
     * Returns true if the specified name is a valid cookie name, otherwise false.
     *
     * @param string $name
     *        The cookie name that should be checked.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public static function isValidCookieName(string $name): bool
    {
        return (bool)preg_match('=^[[:ascii:]]+$=', $name) &&
            !(bool)preg_match('=[[:cntrl:][:space:]\=,;]+=', $name);
    }

    /**
     * Get the modified response.
     *
     * @return ResponseInterface
     * @api usage
     * @since 1.0
     */
    public function getResponse(): ResponseInterface
    {
        if (!$this->cleaned) {
            $this->removeDuplicateCookies();
            $this->cleaned = true;
        }

        return $this->response;
    }

    /**
     * Remove duplicate cookies from the response.
     */
    private function removeDuplicateCookies(): void
    {
        $originalCookies = $this->response->getHeader('Set-Cookie');
        $cleanedCookies = [];

        foreach ($originalCookies as $cookie) {
            $name = explode('=', $cookie, 2)[0];
            $cleanedCookies[$name] = $cookie;
        }

        if (count($originalCookies) === count($cleanedCookies)) {
            return;
        }

        $this->response = $this->response->withoutHeader('Set-Cookie');
        foreach ($cleanedCookies as $cookie) {
            $this->response = $this->response->withAddedHeader('Set-Cookie', $cookie);
        }
    }
}
