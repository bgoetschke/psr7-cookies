<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7Cookies\Unit;

use BjoernGoetschke\Psr7Cookies\HttpResponseCookies;
use DateTime;
use DateTimeImmutable;
use GuzzleHttp\Psr7\Response;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

final class HttpResponseCookiesTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        resetMocks();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        resetMocks();
    }

    private static function createResponse(): ResponseInterface
    {
        return new Response(204);
    }

    /**
     * @return mixed[]
     */
    private static function extractHeaders(ResponseInterface $response): array
    {
        return array_map(
            function ($header) {
                return explode(';', $header);
            },
            $response->getHeader('Set-Cookie'),
        );
    }

    public function testSetSessionCookie(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setSessionCookie('someCookie', 'someValue', '/somePath', 'some.domain', true, true);

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieUntil(): void
    {
        global $mock_time;
        $mock_time = function () {
            return 10000000;
        };

        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieUntil(
            'someCookie',
            'someValue',
            new DateTimeImmutable('@12345678'),
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Expires=' . gmdate(DateTime::COOKIE, 12345678),
                    'Max-Age=2345678',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieUntilNow(): void
    {
        global $mock_time;
        $mock_time = function () {
            return 12345678;
        };

        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieUntil(
            'someCookie',
            'someValue',
            new DateTimeImmutable('@12345678'),
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Expires=' . gmdate(DateTime::COOKIE, 0),
                    'Max-Age=0',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieUntilInThePast(): void
    {
        global $mock_time;
        $mock_time = function () {
            return 123456789;
        };

        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieUntil(
            'someCookie',
            'someValue',
            new DateTimeImmutable('@12345678'),
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Expires=' . gmdate(DateTime::COOKIE, 0),
                    'Max-Age=0',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieFor(): void
    {
        global $mock_time;
        $mock_time = function () {
            return 10000000;
        };

        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieFor(
            'someCookie',
            'someValue',
            2345678,
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Expires=' . gmdate(DateTime::COOKIE, 12345678),
                    'Max-Age=2345678',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieForZeroSeconds(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieFor(
            'someCookie',
            'someValue',
            0,
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testSetCookieForNegativeSeconds(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setCookieFor(
            'someCookie',
            'someValue',
            -1,
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=someValue',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testUnsetCookie(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->unsetCookie(
            'someCookie',
            '/somePath',
            'some.domain',
            true,
            true,
        );

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            1,
            $headers,
        );

        self::assertSame(
            [
                [
                    'someCookie=',
                    'Expires=' . gmdate(DateTime::COOKIE, 0),
                    'Max-Age=0',
                    'Path=/somePath',
                    'Domain=some.domain',
                    'Secure',
                    'HttpOnly',
                ],
            ],
            $headers,
        );
    }

    public function testInvalidCookieNameThrowsException(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid cookie name: name=invalid');

        $cookies->setSessionCookie('name=invalid', 'someValue');
    }

    public function testDuplicateCookiesAreReplaced(): void
    {
        $cookies = new HttpResponseCookies(self::createResponse());

        $cookies->setSessionCookie('cookie1', 'value1'); // will be replaced
        $cookies->setSessionCookie('cookie2', 'value2'); // final
        $cookies->setSessionCookie('cookie3', 'value3'); // will be replaced
        $cookies->setSessionCookie('cookie1', 'value1.1'); // final
        $cookies->unsetCookie('cookie3'); // final
        $cookies->unsetCookie('cookie4'); // will be replaced
        $cookies->setSessionCookie('cookie4', 'value4'); // final

        $headers = self::extractHeaders($cookies->getResponse());

        self::assertCount(
            4,
            $headers,
        );

        self::assertSame(
            [
                [
                    'cookie1=value1.1',
                ],
                [
                    'cookie2=value2',
                ],
                [
                    'cookie3=',
                    'Expires=' . gmdate(DateTime::COOKIE, 0),
                    'Max-Age=0',
                ],
                [
                    'cookie4=value4',
                ],
            ],
            $headers,
        );
    }

    public function testFluentInterface(): void
    {
        global $mock_time;
        $mock_time = function () {
            return 10000000;
        };

        $cookies = new HttpResponseCookies(self::createResponse());

        $headers = self::extractHeaders(
            $cookies
                ->setSessionCookie('cookie1', 'value1')
                ->setCookieFor('cookie2', 'value2', 60)
                ->setCookieUntil('cookie3', 'value3', new DateTimeImmutable('@12345678'))
                ->unsetCookie('cookie2')
                ->getResponse(),
        );

        self::assertCount(
            3,
            $headers,
        );

        self::assertSame(
            [
                [
                    'cookie1=value1',
                ],
                [
                    'cookie2=',
                    'Expires=' . gmdate(DateTime::COOKIE, 0),
                    'Max-Age=0',
                ],
                [
                    'cookie3=value3',
                    'Expires=' . gmdate(DateTime::COOKIE, 12345678),
                    'Max-Age=2345678',
                ],
            ],
            $headers,
        );
    }

    public function testClone(): void
    {
        $cookies1 = new HttpResponseCookies(self::createResponse());
        $cookies2 = clone $cookies1;

        self::assertNotSame(
            $cookies1,
            $cookies2,
        );

        self::assertSame(
            $cookies1->getResponse(),
            $cookies2->getResponse(),
        );
    }
}
