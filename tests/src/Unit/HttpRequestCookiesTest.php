<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7Cookies\Unit;

use BjoernGoetschke\Psr7Cookies\HttpRequestCookies;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

final class HttpRequestCookiesTest extends TestCase
{
    /**
     * @param string[] $cookies
     * @return ServerRequestInterface
     */
    private static function createRequest(array $cookies): ServerRequestInterface
    {
        $body = fopen('php://memory', 'rb');
        self::assertIsResource($body);
        $request = new ServerRequest(
            'GET',
            'http://test.domain/someUri',
            [],
            $body,
            '1.1',
            [],
        );
        return $request->withCookieParams($cookies);
    }

    public function testCorrectCookieValues(): void
    {
        $request = self::createRequest(
            [
                'someCookie' => 'someValue',
                'anotherCookie' => 'anotherValue',
            ],
        );

        $cookies = new HttpRequestCookies($request);

        self::assertTrue(
            $cookies->hasCookie('someCookie'),
        );

        self::assertTrue(
            $cookies->hasCookie('anotherCookie'),
        );

        self::assertFalse(
            $cookies->hasCookie('nonExistingCookie'),
        );

        self::assertSame(
            'someValue',
            $cookies->getValue('someCookie'),
        );

        self::assertSame(
            'anotherValue',
            $cookies->getValue('anotherCookie'),
        );

        self::assertSame(
            '',
            $cookies->getValue('nonExistingCookie'),
        );
    }

    public function testSameRequestReturned(): void
    {
        $request = self::createRequest([]);

        $cookies = new HttpRequestCookies($request);

        self::assertSame(
            $request,
            $cookies->getRequest(),
        );
    }

    public function testClone(): void
    {
        $originalRequest = self::createRequest(
            [
                'someCookie' => 'someValue',
                'anotherCookie' => 'anotherValue',
            ],
        );

        $cookies1 = new HttpRequestCookies($originalRequest);
        $cookies2 = clone $cookies1;

        self::assertNotSame(
            $cookies1,
            $cookies2,
        );

        self::assertSame(
            $cookies1->getRequest(),
            $cookies2->getRequest(),
        );

        self::assertSame(
            $cookies1->getValue('someCookie'),
            $cookies2->getValue('someCookie'),
        );

        self::assertSame(
            $cookies1->getValue('anotherCookie'),
            $cookies2->getValue('anotherCookie'),
        );
    }
}
